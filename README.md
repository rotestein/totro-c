# Totro-C

A C++ implementation based on totro by David A. Wheeler.

Adapted to reduce duplicate entries and allow quick modification of consonant weights.

For original code: http://www.dwheeler.com/totro.html

# Usage

Call Init() to initialize rand().

Call RandomName(minsyllables, maxsyllables) to generate a random name.

# Implementation Notes
totro.cpp contains a list of possible vowels, followed by list of possible consonants.

In both lists, weight / total wieght determines the likelihood of that selection.

The second parameter indicates if the syllable can occur
at the beginning, middle, or ending of a name. To allow for more than one position,
take the logical or of the positions.

There MUST be a possible vowel and possible consonant for any
possible position; if you want to have "no vowel at the end", use
{'', END, 1} and make sure no other vowel includes "can be at end".
