#include <string>
#include <iostream>
#include <cstdlib>

#include "totro.hpp"

int main(int argc, char * argv[]) {
  int min = 3, max = 5, n = 1, i;
  switch (argc - 1) {
    case 3:
      n = atoi(argv[3]);
    case 2:
      max = atoi(argv[2]);
    case 1:
      min = atoi(argv[1]);
  }

  Init();

  for (i = 0; i < n; i++) {
    std::cout << RandomName(min, max) << std::endl;
  }
  return 0;
}
