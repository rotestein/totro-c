#include <string>
#include <cmath>
#include <cstdlib>
#include <iostream>
using namespace std;

#define END 1
#define BEG 2
#define MID 4
#define ALL 7

typedef struct {
  string syllable;
  int    position;
  int    weight;
} syllable_t;


// List of possible vowels.
const int nVowels = 25;
syllable_t vowels[] = {
  {"a", ALL, 12}, {"e", ALL, 12}, {"i", ALL, 12}, {"o", ALL, 12}, {"u", ALL, 12}, 
  {"ae", ALL, 1}, {"ai", ALL, 1}, {"ao", ALL, 1}, {"au", ALL, 1}, {"aa", ALL, 1},
  {"ea", ALL, 1}, {"eo", ALL, 1}, {"eu", ALL, 1}, {"ee", ALL, 1},
  {"ia", ALL, 1}, {"io", ALL, 1}, {"iu", ALL, 1}, {"ii", ALL, 1},
  {"oa", ALL, 1}, {"oe", ALL, 1}, {"oi", ALL, 1}, {"ou", ALL, 1}, {"oo", ALL, 1},
  {"'", MID, 1},
  {"y", ALL, 1}
};

// List of possible consonants.
const int nConsonants = 21 + 31;
syllable_t consonants[] = {
  {"b", ALL, 3},  {"c", ALL, 3},  {"d", ALL, 3},  
  {"f", ALL, 3},  {"g", ALL, 3},  {"h", ALL, 3}, 
  {"j", ALL, 3},  {"k", ALL, 3},  {"l", ALL, 3},  
  {"m", ALL, 3},  {"n", ALL, 3},  {"p", ALL, 3}, 
  {"qu", BEG | MID},  {"r", ALL, 3}, {"s", ALL, 3},  
  {"t", ALL, 3},  {"v", ALL, 3},  {"w", ALL, 3},
  {"x", ALL, 1},  {"y", ALL, 1},  {"z", ALL, 1}, 

  // Blends, sorted by second character:
  {"sc", ALL, 1},

  {"ch", ALL, 1},  {"gh", ALL, 1},  {"ph", ALL, 1}, 
    {"sh", ALL, 1},  {"th", ALL, 1}, {"wh", BEG | MID, 1},

  {"ck", END | MID, 1},  {"nk", END | MID, 1},  {"rk", END | MID, 1}, 
    {"sk", ALL, 1},  {"wk", 0, 1},

  {"cl", BEG | MID, 1},  {"fl", BEG | MID, 1},  {"gl", BEG | MID, 1}, 
    {"kl", BEG | MID, 1},  {"ll", BEG | MID, 1}, {"pl", BEG | MID, 1}, 
{"sl", BEG | MID, 1},

  {"br", BEG | MID, 2},  {"cr", BEG | MID, 1},  {"dr", BEG | MID, 2},  
    {"fr", BEG | MID, 2},  {"gr", BEG | MID, 2},  {"kr", BEG | MID, 2}, 
    {"pr", BEG | MID, 1},  {"sr", BEG | MID, 1},  {"tr", BEG | MID, 1},

  {"ss", END | MID, 1}, {"st", ALL, 1},  {"str", BEG | MID, 1},
};

void Init() {
  srand(time(0));
}

int rolldie(int minvalue, int maxvalue) {
  int result;
  float r;
  while (1) {
    r = (float) rand() / (float) RAND_MAX;
    result = floor(r * (maxvalue - minvalue + 1) + minvalue); 
    if ((result >= minvalue) && (result <= maxvalue)) { 
     return result;
    }
  }
}


// Return a random value between minvalue and maxvalue, inclusive,
// with equal probability.
syllable_t GetSyllable(syllable_t syllables[], int len) {
  int total, i, r;
  for (i = 0, total = 0; i < len; i++) total += syllables[i].weight;
  //cout << "weights " << total << endl;

  r = rolldie(0, total -1);

  for (i = 0; r > 0; i++) r -= syllables[i].weight;

  //cout << "i " << i << endl;
     
  return syllables[i];
}

// Create a random name.  It must have at least between minsyl and maxsyl
// number of syllables (inclusive).
string RandomName(int minsyl, int maxsyl) {
  syllable_t data;
  string genname = "";         // this accumulates the generated name.
  int leng = rolldie(minsyl, maxsyl); // Compute number of syllables in the name
  int isvowel = rolldie(0, 1); // randomly start with vowel or consonant
  int i;
  for (i = 1; i <= leng; i++) { // syllable #. Start is 1 (not 0)
    do {
      if (isvowel) {
        data = GetSyllable(vowels, nVowels);
      } else {
        data = GetSyllable(consonants, nConsonants);
      }
      if ( i == 1) { // first syllable.
        if (data.position & 2) {
          break;
        }
      } else if (i == leng) { // last syllable.
        if (data.position & 1) {
          break;
        }
      } else { // middle syllable.
        if (data.position & 4) {
          break;
        }
      }
    } while (1);
    genname += data.syllable;
    isvowel = 1 - isvowel; // Alternate between vowels and consonants.
  }
  // Initial caps:
  genname = (char)toupper(genname[0]) + genname.substr(1, genname.length());
  //genname = (genname.slice(0,1)).toUpperCase() + genname.slice(1);
  return genname;
}
